<?php

namespace app;

use app\actions\RegisterUser;
use app\components\db\Db;
use app\components\flashmessage\FlashMessage;
use app\components\session\Session;
use app\components\twig\Twig;
use app\records\User;
use app\usecases\registration\RegistrationFormValidator;

class WebApplication
{
    public function __construct(Config $config)
    {
        $this->bootstrap();
        Db::setConfig($config);
    }

    public function run(): void
    {
        $response = $this->runAction();
        echo $response;
    }

    private function runAction(): string
    {
        $route = $this->getRoute();

        if ($route === '/') {
            return $this->actionMainPage();
        }

        if ($route === '/feed') {
            return $this->actionNews();
        }

        if ($route === '/register') {
            return $this->actionRegister();
        }

        if ($route === '/user-page') {
            return $this->actionUserPage();
        }

        if ($route === '/user-list') {
            return $this->actionUserList();
        }

        return $this->actionNotFound();
    }

    private function actionMainPage(): string
    {
        return $this->render('main-page', ['name' => 'Fabien']);
    }

    private function actionNews(): string
    {
        return 'Общая лента новостей';
    }

    private function actionNotFound(): string
    {
        $this->setNotFoundHeader();
        return $this->errorResponse('Ой. Страница не найдена. Нам очень жаль :(');
    }

    private function errorResponse(string $message, int $code = 200): string
    {
        $this->setStatusCode($code);
        return $this->render('error-page', ['message' => $message]);
    }

    private function setNotFoundHeader(): void
    {
        $this->setStatusCode(404);
    }

    private function getRoute(): string
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }

    private function actionRegister(): string
    {
        $formData = $this->getPostData('registrationForm');

        if (empty($formData)) {
            return $this->render('registration-page');
        }

        if (!empty($formData)) {
            try {
                $validator = new RegistrationFormValidator();
                $validator->validate($formData);
                if (!$validator->isSuccess()) {
                    return $this->render('registration-page', [
                        'form' => $formData,
                        'validationErrors' => $validator->getErrors(),
                    ]);
                }

                $action = new RegisterUser(
                    $formData['login'],
                    $formData['first_name'],
                    $formData['last_name'],
                    $formData['password'],
                    (int)$formData['age'],
                    $formData['gender'],
                    $formData['hobbies'],
                    $formData['city']
                );
                $action->execute();

                (new FlashMessage())->success('Вы зарегистрированы!');
            } catch (\Exception $exception) {
                return $this->errorResponse($exception->getMessage());
            }
        }

        return $this->redirectResponse('/');
    }

    private function actionUserPage(): string
    {
        $login = $this->getGetData('user');
        if (($login === null) || !is_string($login) || ($login === '')) {
            return $this->errorResponse('Не указан логин пользователя', 400);
        }

        $user = User::findByLogin($login);
        if ($user === null) {
            return $this->errorResponse("Не найден пользователь: {$login}", 404);
        }

        return $this->render('user-page', [
            'user' => $user,
        ]);
    }

    private function actionUserList(): string
    {
        $users = User::findLastRegistered(10);
        $usersCount = User::getCount();

        return $this->render('user-list', [
            'users' => $users,
            'usersCount' => $usersCount,
        ]);
    }

    private function redirectResponse(string $path): string
    {
        header("Location: {$path}", true, 302);
        exit();
    }

    private function getPostData(string $key)
    {
        $post = $_POST;

        if (!array_key_exists($key, $post)) {
            return null;
        }

        return $post[$key];
    }

    private function getGetData(string $key)
    {
        $get = $_GET;

        if (!array_key_exists($key, $get)) {
            return null;
        }

        return $get[$key];
    }

    private function setStatusCode(int $code): void
    {
        http_response_code($code);
    }

    private function render(string $templateName, array $params = []): string
    {
        return (new Twig())
            ->addGlobal('flash', new FlashMessage())
            ->render($templateName, $params);
    }

    private function bootstrap(): void
    {
        (new Session())->bootstrap();
    }
}