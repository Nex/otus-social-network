<?php

namespace app\usecases\registration;

use app\records\User;
use Valitron\Validator;

/**
 * Class RegistrationFormValidator
 * @package app\usecases\registration
 */
class RegistrationFormValidator
{
    private $errors = [];

    public function validate(array $fields): void
    {
        $this->errors = [];

        $trimmed = [];
        foreach ($fields as $key => $field) {
            $trimmed[$key] = trim($field);
        }

        if ($this->existsUserWithLogin($trimmed['login'])) {
            $this->errors['login'] = 'Это имя пользователя уже занято. Попробуйте выбрать другое.';
        }

        Validator::lang('ru');

        $v = new Validator($trimmed);
        $v->rules([
            'required' => [
                ['login'],
                ['password'],
                ['first_name'],
                ['last_name'],
                ['password'],
                ['age'],
                ['gender'],
                ['city'],
            ],
            'lengthMax' => [
                ['login', 255],
                ['password', 255],
                ['first_name', 100],
                ['last_name', 100],
                ['city', 255],
                ['hobbies', 255],
            ],
            'slug' => [
                ['login']
            ],
            'integer' => [
                ['age', true],
            ],
            'min' => [
                ['age', 1],
            ],
            'max' => [
                ['age', 200],
            ],
            'optional' => [
                ['hobbies'],
            ],
            'in' => [
                ['gender', ['m', 'f']],
            ],
        ]);

        $v->validate();

        foreach ($v->errors() as $field => $fieldErrors) {
            if (count($fieldErrors) === 0) {
                continue;
            }
            $this->errors[$field] = $fieldErrors[0];
        }
    }

    public function isSuccess(): bool
    {
        return count($this->errors) === 0;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    private function existsUserWithLogin(string $login): bool
    {
        return User::findByLogin($login) !== null;
    }
}