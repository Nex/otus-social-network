<?php

namespace app\records;

use app\components\db\Db;
use app\components\id\Id;

/**
 * Class User
 * @package app\records
 */
class User
{
    private const GENDER_INT_MALE = 1;

    private const GENDER_INT_FEMALE = 0;

    private const GENDER_STRING_MALE = 'm';

    private const GENDER_STRING_FEMALE = 'f';

    public $id;

    public $login;

    public $firstName;

    public $lastName;

    public $fullName;

    public $passwordHash;

    public $age;

    public $gender;

    public $hobbies;

    public $city;

    private function __construct(
        ?string $id,
        string $login,
        string $firstName,
        string $lastName,
        string $passwordHash,
        int $age,
        string $gender,
        string $hobbies,
        string $city
    )
    {
        $this->id = $id;
        $this->login = $login;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->passwordHash = $passwordHash;
        $this->fullName = "{$firstName} {$lastName}";
        $this->age = $age;
        $this->gender = $gender;
        $this->hobbies = $hobbies;
        $this->city = $city;
    }

    public static function fromRegistration(
        string $login,
        string $firstName,
        string $lastName,
        string $hash,
        int $age,
        string $gender,
        string $hobbies,
        string $city
    ): self
    {
        return new self(
            null,
            $login,
            $firstName,
            $lastName,
            $hash,
            $age,
            $gender,
            $hobbies,
            $city
        );
    }

    public static function fromDb(array $record): self
    {
        return new self(
            $record['id'],
            $record['login'],
            $record['first_name'],
            $record['last_name'],
            $record['password_hash'],
            (int)$record['age'],
            self::genderToString($record['gender']),
            $record['hobbies'],
            $record['city']
        );
    }

    private static function genderToString(int $gender): string
    {
        return $gender === self::GENDER_INT_MALE
            ? self::GENDER_STRING_MALE
            : self::GENDER_STRING_FEMALE;
    }

    public static function getCount(): int
    {
        return (int)Db::selectQueryScalar("SELECT COUNT(*) FROM `user`");
    }

    private static function genderToInt(string $gender): int
    {
        return $gender === self::GENDER_STRING_MALE
            ? self::GENDER_INT_MALE
            : self::GENDER_INT_FEMALE;
    }

    public function save(): void
    {
        if ($this->id !== null) {
            Db::update('user',
                [
                    'login' => $this->login,
                    'first_name' => $this->firstName,
                    'last_name' => $this->lastName,
                    'full_name' => $this->fullName,
                    'password_hash' => $this->passwordHash,
                    'age' => $this->age,
                    'gender' => self::genderToInt($this->gender),
                    'hobbies' => $this->hobbies,
                    'city' => $this->city,
                ], [
                    'id' => $this->id
                ]);
        } else {
            Db::insert('user', [
                    'id' => Id::generate(),
                    'login' => $this->login,
                    'first_name' => $this->firstName,
                    'last_name' => $this->lastName,
                    'full_name' => $this->fullName,
                    'password_hash' => $this->passwordHash,
                    'age' => $this->age,
                    'gender' => self::genderToInt($this->gender),
                    'hobbies' => $this->hobbies,
                    'city' => $this->city,
                ]);
        }
    }

    public static function findByLogin(string $login): ?self
    {
        $record = Db::selectRow('user', [
            'login' => $login,
        ]);

        if (!is_array($record)) {
            return null;
        }

        return self::fromDb($record);
    }

    public static function findLastRegistered(int $limit): array
    {
        $records = Db::selectAll('user', [], 'id DESC', $limit);

        $users = [];
        foreach ($records as $record) {
            $users[] = self::fromDb($record);
        }

        return $users;
    }

    public function delete(): void
    {
        Db::delete('user', [
            'id' => $this->id,
        ]);
    }
}